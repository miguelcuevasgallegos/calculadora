package com.anthosdev.calculator.controller;

import com.anthosdev.calculator.resources.datasource.entities.SimpleCalculatorEntities;
import com.anthosdev.calculator.usecase.dividir.DividirUseCase;
import com.anthosdev.calculator.usecase.multiplicar.MultiplicarUseCase;
import com.anthosdev.calculator.usecase.potencia.PotenciaUseCase;
import com.anthosdev.calculator.usecase.raizcuadrada.RaizCuadradaUseCase;
import com.anthosdev.calculator.usecase.resta.RestaUseCase;
import com.anthosdev.calculator.usecase.suma.SumaUseCase;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "/calculator")
public class GeneralController {
    private final SumaUseCase sumaUseCase;
    private final RestaUseCase restaUseCase;
    private final MultiplicarUseCase multiplicarUseCase;
    private final DividirUseCase dividirUseCase;
    private final RaizCuadradaUseCase raizCuadradaUseCase;
    private final PotenciaUseCase potenciaUseCase;

    public GeneralController(SumaUseCase sumaUseCase, RestaUseCase restaUseCase, MultiplicarUseCase multiplicarUseCase, DividirUseCase dividirUseCase, RaizCuadradaUseCase raizCuadradaUseCase, PotenciaUseCase potenciaUseCase) {
        this.sumaUseCase = sumaUseCase;
        this.restaUseCase = restaUseCase;
        this.multiplicarUseCase = multiplicarUseCase;
        this.dividirUseCase = dividirUseCase;
        this.raizCuadradaUseCase = raizCuadradaUseCase;
        this.potenciaUseCase = potenciaUseCase;
    }

    @PostMapping ("/suma")
    public ResponseEntity<Integer>suma(@RequestBody SimpleCalculatorEntities simpleCalculatorEntities){
        return ResponseEntity.ok(sumaUseCase.resultadoSuma(simpleCalculatorEntities));
    }
    @PostMapping("/resta")
    public ResponseEntity<Integer>resta(@RequestBody SimpleCalculatorEntities simpleCalculatorEntities){
        return ResponseEntity.ok(restaUseCase.resultadoResta(simpleCalculatorEntities));
    }
    @PostMapping("/multiplicar")
    public ResponseEntity<Integer>multiplicar(@RequestBody SimpleCalculatorEntities simpleCalculatorEntities){
        return ResponseEntity.ok(multiplicarUseCase.resultadoMulti(simpleCalculatorEntities));
    }
    @PostMapping("/dividir")
    public ResponseEntity<Integer>dividir(@RequestBody SimpleCalculatorEntities simpleCalculatorEntities){
        return ResponseEntity.ok(dividirUseCase.resultadoDivision(simpleCalculatorEntities));
    }
    @PostMapping("/raizcuadrada")
    public ResponseEntity<Integer>raizcuadrada(@RequestBody SimpleCalculatorEntities simpleCalculatorEntities){
        return ResponseEntity.ok(raizCuadradaUseCase.resultadoRaizCuadrada(simpleCalculatorEntities));
    }
    @PostMapping("/potencia")
    public ResponseEntity<Integer>potencia(@RequestBody SimpleCalculatorEntities simpleCalculatorEntities){
        return ResponseEntity.ok(potenciaUseCase.resulatadoPotencia(simpleCalculatorEntities));
    }
}
