package com.anthosdev.calculator.usecase.resta;

import com.anthosdev.calculator.resources.datasource.entities.SimpleCalculatorEntities;
import org.springframework.stereotype.Service;

@Service
public class RestaUseCase {
    public int resultadoResta(SimpleCalculatorEntities simpleCalculatorEntities){
        return simpleCalculatorEntities.getNumero1() - simpleCalculatorEntities.getNumero2();
    }
}
