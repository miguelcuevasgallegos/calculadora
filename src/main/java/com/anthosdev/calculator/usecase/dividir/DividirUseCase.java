package com.anthosdev.calculator.usecase.dividir;

import com.anthosdev.calculator.resources.datasource.entities.SimpleCalculatorEntities;
import org.springframework.stereotype.Service;

@Service
public class DividirUseCase {
    public int resultadoDivision(SimpleCalculatorEntities simpleCalculatorEntities){
        return simpleCalculatorEntities.getNumero1() / simpleCalculatorEntities.getNumero2();
    }
}
