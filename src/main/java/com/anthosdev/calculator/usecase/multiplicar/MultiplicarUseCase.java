package com.anthosdev.calculator.usecase.multiplicar;

import com.anthosdev.calculator.resources.datasource.entities.SimpleCalculatorEntities;
import org.springframework.stereotype.Service;

@Service
public class MultiplicarUseCase {
    public int resultadoMulti(SimpleCalculatorEntities simpleCalculatorEntities){
        return simpleCalculatorEntities.getNumero1() * simpleCalculatorEntities.getNumero2();
    }
}
