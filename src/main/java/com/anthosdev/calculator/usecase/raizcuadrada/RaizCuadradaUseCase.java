package com.anthosdev.calculator.usecase.raizcuadrada;

import com.anthosdev.calculator.resources.datasource.entities.SimpleCalculatorEntities;
import org.springframework.stereotype.Service;

@Service
public class RaizCuadradaUseCase {

    public int resultadoRaizCuadrada(SimpleCalculatorEntities simpleCalculatorEntities){
        return (int) Math.sqrt(simpleCalculatorEntities.getNumero1());
    }
}
