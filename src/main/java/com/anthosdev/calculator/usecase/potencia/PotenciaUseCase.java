package com.anthosdev.calculator.usecase.potencia;

import com.anthosdev.calculator.resources.datasource.entities.SimpleCalculatorEntities;
import org.springframework.stereotype.Service;

@Service
public class PotenciaUseCase {
    public int resulatadoPotencia(SimpleCalculatorEntities simpleCalculatorEntities){
        return (int) Math.pow(simpleCalculatorEntities.getNumero1(), simpleCalculatorEntities.getNumero2());
    }
}
