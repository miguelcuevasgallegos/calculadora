package com.anthosdev.calculator.usecase.suma;

import com.anthosdev.calculator.resources.datasource.entities.SimpleCalculatorEntities;
import org.springframework.stereotype.Service;

@Service
public class SumaUseCase {

    public int resultadoSuma(SimpleCalculatorEntities simpleCalculatorEntities){
        return simpleCalculatorEntities.getNumero1() + simpleCalculatorEntities.getNumero2();
    }
}
